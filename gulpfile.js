(function() {
    'use strict';


    var	gulp = require('gulp')
    	,jshint = require('gulp-jshint')
    	,jshintStylish = require('jshint-stylish')
    	,csslint = require('gulp-csslint')
    	// ,autoprefixer = require('gulp-autoprefixer')
    	,sass = require('gulp-sass')
        ,browserSync = require('browser-sync')
        ,cssmin = require('gulp-cssmin')
        ,rename = require('gulp-rename')


    gulp.task('default', function() {
    	gulp.start('sass:watch');
    });


    gulp.task('sass', function () {
      gulp.src('./sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./ficticia-videos/css'));
    });
     
    gulp.task('sass:watch', function () {
      gulp.watch('./sass/**/*.scss', ['sass']);
    });
     
    gulp.task('cssmin', function () {
        gulp.src('./ficticia-videos/css/*.css')
            .pipe(cssmin())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest('./ficticia-videos/css'));
    });


    gulp.task('server', function() {
        browserSync.init({
            server: {
                baseDir: 'ficticia-videos/'
            },
            reloadDelay: 800
        });

        gulp.watch('ficticia-videos/**/*').on('change', browserSync.reload);

        gulp.watch('ficticia-videos/app/**/*.js').on('change', function(event) {
            console.log("Linting " + event.path);
            gulp.src(event.path)
                .pipe(jshint())
                .pipe(jshint.reporter(jshintStylish));
        });

        gulp.watch('ficticia-videos/css/**/*.css').on('change', function(event) {
            console.log("Linting " + event.path);
            gulp.src(event.path)
                .pipe(csslint())
                .pipe(csslint.reporter());
        });    

    });


})()