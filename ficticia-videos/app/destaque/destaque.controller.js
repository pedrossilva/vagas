(function() {
    'use strict';

    angular
        .module('fvapp')
        .controller('DestaqueController', DestaqueController);

    DestaqueController.$inject = ['$scope', '$rootScope', '$http', '$timeout', 'youtubeService'];


    /**
     * Manter a "ponte" entre 'dados' e 'view' da página destaque.
     *
     * @controller
     * @this {DestaqueController}
     * @params {dependencies} 
     */
    /* @ngInject */
    function DestaqueController($scope, $rootScope, $http, $timeout, youtubeService) {
        var vm = this;
        vm.emdestaque = {titulo: 'Vídeo em destaque'};
        vm.maisvideos = {titulo: '+ Vídeos'};
        vm.videos = youtubeService.getVideos('destaque');
        vm.videoDestaque = vm.videos[0] || {};
        vm.query = '';

        if(vm.videos.length == 0) getYTVideos(4, true);

        /**
         * Função do scopo para acionar o botão "carregar mais vídeos".
         * chama a função getYTVideos para adicionar mais videos ao escopo.
         */
        vm.maisVideos = function() {
            getYTVideos(4);
        }

        /**
         * Escuta o evento "buscavideo" definido pelo $scope.$broadcast do controller pai.
         * define a variavel query do escopo com o valor (query) informado no campo busca.
         */
        $scope.$on('buscavideo', function(event, query) {
            vm.query = query;
        });

        /**
         * Após o carregamento do scopo inicia o plugin "Tipped" para tooltips.
         */
        $timeout(function() {
            Tipped.create('.inline');
        }, 0);

        ////////////////

        /**
         * Obter os IDs dos vídeos do youtube.
         *
         * @param {number} num Informar a quantidade máxima de vídeos a serem buscados.
         * @param {boolean} first Informar se é a primeira requisição (ou seja, ao carregar a página) (true ou false).
         */
        function getYTVideos(num, first) {
            first = (typeof first !== 'boolean') ? false : first;
            var videosIdPromise = youtubeService.getIdVideos('destaque', num, first);

            videosIdPromise.then(function(result) {
                if(result.status == 200) {
                    var videosPromise = youtubeService.getYTVideos('destaque', result.data.items);
                    youtubeService.setPageToken('destaque', result.data.nextPageToken);
                    videosPromise.then(thenVideos);
                } 
                else console.error('Error getIdVideos(), status:'+result.status+': '+result.statusText);
            });
        }

        /**
         * Callback do promise da função "getYTVideos" do serviço "youtubeService".
         * passo final para apresentar os novos videos obtidos do youtube.
         *
         * @param {object} result Retorno 'data' da requisição que obtem os vídeos detalhados.
         */
        function thenVideos(result) {
            if(result.status == 200) {
                youtubeService.setVideos('destaque', result.data.items);
                vm.videos = youtubeService.getVideos('destaque');
                if(typeof vm.videoDestaque.id == 'undefined') vm.videoDestaque = vm.videos[0];
                $timeout(function() {
                    vm.loading = false;
                }, 1000);
            } 
            else console.error('Error getYTVideos(), status:'+result.status+': '+result.statusText);
        }
    }
})(angular);