(function() {
    'use strict';

    angular
        .module('fvapp')
        .service('youtubeService', youtubeService);

    youtubeService.$inject = ['$http'];

    /* @ngInject */
    function youtubeService($http) {
        this.getIdVideos = getIdVideos;
        this.getYTVideos = getYTVideos;
        this.setPageToken = setPageToken;
        this.getVideos = getVideos;
        this.setVideos = setVideos;

        ////////////////
    	var baseUrl = 'https://www.googleapis.com/youtube/v3/';
    	var apiKey = 'AIzaSyDCIytE0jvg0wo29mIN0ehxGP7dtPzebSs';
    	var channelId = 'UCbAiIkPobUFy71zlmwVw3jw';
        var idVideos = [];
        var nextPageToken = [];
        var videos = {};

        /**
         * Definindo o token (fornecido na requisição dos vídeos) da próxima página.
         *
         * @param {string} page Informar a página que está solicitando (ex: 'videos' ou 'destaque').
         * @param {string} pageToken Informar o token (fornecido na requisição dos vídeos) a ser definido.
         */
        function setPageToken(page, pageToken) {
        	nextPageToken[page] = pageToken;
        }

        /**
         * Obter os IDs dos vídeos do youtube.
         *
         * @param {string} page Informar a página que está solicitando (ex: 'videos' ou 'destaque').
         * @param {number} maxResults Informar a quantidade máxima de vídeos a serem buscados.
         * @param {boolean} first Informar se é a primeira requisição (ou seja, ao carregar a página) (true ou false).
         * @return {promise} retorna o "promise" da requisição HTTP.
         */
        function getIdVideos(page, maxResults, first) {
        	maxResults = maxResults || 10;
        	if(typeof nextPageToken[page] == 'undefined') nextPageToken[page] = '';
        	var YTUrl = baseUrl+'search?'
        		+(nextPageToken[page] != '' && !first ? '&pageToken='+nextPageToken[page]+'&' : '')
        		+'key='+apiKey
        		+'&channelId='+channelId
        		+'&part=id'
        		+'&order=date'
        		+'&maxResults='+maxResults
        		+"&callback=JSON_CALLBACK";
			return $http.jsonp(YTUrl);
        }

        /**
         * Obter os dados detalhados dos vídeos do youtube.
         *
         * @param {string} page Informar a página que está solicitando (ex: 'videos' ou 'destaque').
         * @param {array} items Informar o array de IDs obtidos pela função getIdVideos.
         */
        function getYTVideos(page, items) {
        	if(typeof idVideos[page] == 'undefined') idVideos[page] = [];
        	var arr = [];
        	for(var key in items) arr.push(items[key].id.videoId);
        	idVideos[page].push(arr);
        	var key = idVideos[page].length-1;
        	var videosId = idVideos[page][key].join(',');
        	var YTUrl = baseUrl+'videos?'
        		+'part=id,snippet,statistics,contentDetails'
        		+'&fields=items(id,snippet(publishedAt,thumbnails(medium),title,description),contentDetails(duration),statistics(viewCount))'
        		+'&id='+videosId
        		+'&key='+apiKey
        		+"&callback=JSON_CALLBACK";
        	return $http.jsonp(YTUrl);
        }

        /**
         * Definir e manter o valor da variável video deste servico.
         *
         * @param {string} page Informar a página que está solicitando (ex: 'videos' ou 'destaque').
         * @param {array} svideos Informar o array de objetos com os dados detalhados dos vídeos.
         */
        function setVideos(page, svideos) {
            if(typeof videos[page] === 'undefined') videos[page] = svideos;
            else {
                for(var idx in svideos) {
                    videos[page].push(svideos[idx]);
                }
            }
        }

        /**
         * Obter o valor da variável video deste servico.
         *
         * @param {string} page Informar a página que está solicitando (ex: 'videos' ou 'destaque').
         */
        function getVideos(page) {
            return videos[page] || [];
        }
    }
})(angular);