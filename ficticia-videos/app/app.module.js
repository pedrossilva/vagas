(function() {
    'use strict';

    angular
        .module('fvapp', [
        	'ngRoute',
        	'angularMoment'
        ]);

})();