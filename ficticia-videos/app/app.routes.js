(function() {
    'use strict';

    angular
        .module('fvapp')
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {
    	$routeProvider
    		.when('/destaques', {
                templateUrl: 'app/destaque/destaque.html',
                controller: 'DestaqueController',
                controllerAs: 'destaque'
            })
            .when('/videos', {
    			templateUrl: 'app/videos/videos.html',
    			controller: 'VideosController',
    			controllerAs: 'videos'
    		})
            .otherwise('/destaques');
    }

})();