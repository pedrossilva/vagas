(function() {
    'use strict';

    angular
        .module('fvapp')
        .filter('kFormatter', kFormatter)
        .filter('youtubeEmbed', youtubeEmbed)
        .filter('cut', cut)
        .filter('fdate', fdate)
        .filter('fNum', fNum)
        .filter('ptTime', ptTime)
        .filter('zeroFill', zeroFill);


    /**
     * Formatar com "k" para abreviação de números maiores que 1000.
     *
     * @param {number} num Informar o número a ser formatado.
     */
    function kFormatter() {
        return kFormatterFilter;

        function kFormatterFilter(num) {
        	return num > 999 ? (num/1000).toFixed(1) + 'k' : num;
        }
    }

    /**
     * Trata a URL do youtube e gera um objeto/url confiável para o angular.js.
     *
     * @param {string} url Informar a url ou código do vídeo do youtube.
     * @param {number} rel (0 ou 1) Apresentar vídeos relacionados ?.
     * @param {number} controls (0 ou 1) Apresentar controles no vídeo ?.
     * @param {number} showinfo (0 ou 1) Apresentar informações ex: título do vídeo ?.
     * @param {number} autoplay (0 ou 1) Iniciar automaticamente o vídeo ?.
     */
    function youtubeEmbed($sce) {
        //rel=0&amp;controls=0&amp;showinfo=0
        return function(url, rel, controls, showinfo, autoplay) { 
            if(typeof url !== 'string' ||  url == '') return;
            if(typeof rel === 'undefined') rel = 0;
            if(typeof controls === 'undefined') controls = 1;
            if(typeof showinfo === 'undefined') showinfo = 1;
            if(typeof autoplay === 'undefined') autoplay = 0;
            url = url.trim();
            var uri = "http://www.youtube.com/embed/";
            if (url.length <= 11) {
                uri += url;
            } else {
                var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
                var match = url.match(regExp);
                var matchID = "";
                try {matchID = match[7].trim() } catch(exc){}
                uri += (match&&matchID.length==11) ? matchID : url;
            }
            var urlParams = "?rel="+rel+"&amp;controls="+controls+"&amp;showinfo="+showinfo+"&amp;autoplay="+autoplay;
            return $sce.trustAsResourceUrl(uri+urlParams);
        };
    }

    /**
     * Recorta a string na quantidade de caracteres desejado.
     *
     * @param {string} value Informar a string a ser formatada.
     * @param {boolean} wordwise cortar após o término da palavra ?.
     * @param {number} max Quantidade de caracteres a ser limitado.
     * @param {string} tail Informar o que escrever após o corte (default: '…').
     */
    function cut() {
        // usage: {{some_text | cut:true:100:' ...'}}
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || '…');
        };
    }

    /**
     * Inserir o "de" nas datas de padrão "01 Janeiro 2016".
     *
     * @param {string} dateString Informar a data no padrão "01 Janeiro 2016".
     */
    function fdate() {
        return function(dateString) {
            return dateString.replace(/ /g, ' de ');
        };
    }

    /**
     * Formata o número com ponto ex: 1000 -> 1.000 .
     *
     * @param {number} num Informar o número a ser formatado.
     */
    function fNum(){
        return function(num) {
            var rx=  /(\d+)(\d{3})/;
            return String(num).replace(/^\d+/, function(w){
                while(rx.test(w)){
                    w= w.replace(rx, '$1,$2');
                }
                return w;
            });
        }
    }

    /**
     * Formata tempo de duração padrão "PT5M33S" (comum no youtube) para "5:33".
     *
     * @param {string} str Informar o tempo de duração a ser formatado.
     */
    function ptTime() {
        return function(str) {
            var time = str.match(/^PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?$/);
            var h = (typeof time[2] == 'undefined') ? '0' : time[2];//zeroFill()(time[2], 2);
            var m = (typeof time[3] == 'undefined') ? '00' : time[3];
            var min = zeroFill()(m, 2);
            return h+':'+min;
        }
    }

    /**
     * Formata número adicionando zeros à esquerda.
     *
     * @param {number} n Informar o número a ser formatado.
     * @param {number} width Informar o tamanho do número em casas, ex: 3: 1 -> 001.
     */
    function zeroFill() {
        return function pad(n, width, z) {
          z = z || '0';
          n = n + '';
          return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }

})(angular);