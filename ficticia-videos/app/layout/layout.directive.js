(function() {
    'use strict';

    angular
        .module('fvapp')
        .directive('fvHeader', fvHeader);

    fvHeader.$inject = ['$http'];

    /* @ngInject */
    function fvHeader($http) {

        /**
         * Adicionar o header, comum em todas as páginas.
         */
        var directive = {
            templateUrl: 'app/layout/header.html',
            restrict: 'EA'
        };
        return directive;

    }

})();