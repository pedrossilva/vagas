(function() {
    'use strict';

    angular
        .module('fvapp')
        .controller('FVController', FVController);

    FVController.$inject = ['$scope', '$rootScope', '$http'];


    /**
     * Manter a "ponte" entre 'dados' e 'view' sendo o controller pai.
     *
     * @controller
     * @this {FVController}
     * @params {dependencies} 
     */
    /* @ngInject */
    function FVController($scope, $rootScope, $http) {
        var vm = this;
        vm.title = 'FVController';
        vm.setActiveBusca = setActiveBusca;
        vm.search = search;
        vm.query = '';


        ////////////////

        /**
         * Apresenta e mantem o foco no input de busca (ex: ao clicar no botão ícone de lupa).
         */
        function setActiveBusca() {
            vm.activeBusca = !vm.activeBusca;
            $('#input-busca').focus();
            $('#input-busca').unbind('keypress').keypress(function (e) {
                if(e.which == 13) $('#input-busca').val('').blur();
            });
            $('#input-busca').unbind('blur').blur(function (e) {
                $('#input-busca').val('');
            });
        }

        /**
         * Define o evento "buscavideo" para que as controllers filhas possam escutá-lo.
         */
        function search() {
            $scope.$broadcast('buscavideo', vm.query);
        };

    }
})();