(function() {
    'use strict';

    angular
        .module('fvapp')
        .controller('VideosController', VideosController);

    VideosController.$inject = ['$scope', '$rootScope', '$http', '$timeout', '$filter', 'youtubeService'];


    /**
     * Manter a "ponte" entre 'dados' e 'view' da página vídeos.
     *
     * @controller
     * @this {VideosController}
     * @params {dependencies} 
     */
    /* @ngInject */
    function VideosController($scope, $rootScope, $http, $timeout, $filter, youtubeService) {
        var vm = this;
        vm.titulo = 'Todos os vídeos do Canal';
        vm.videos = youtubeService.getVideos('videos');
        vm.query = '';

        if(vm.videos.length === 0) getYTVideos(12, true);

        /**
         * Após o carregamento do scopo inicia o plugin "Tipped" para tooltips.
         */
        $timeout(function() {
            Tipped.create('.inline');
        }, 0);

        /**
         * Função do scopo para acionar o botão "carregar mais vídeos".
         * chama a função getYTVideos para adicionar mais videos ao escopo.
         */
        vm.maisVideos = function() {
            getYTVideos(12);
            maisVideosScroll=true;
            videos.loading=true;
        };

        /**
         * Escuta o evento "buscavideo" definido pelo $scope.$broadcast do controller pai.
         * define a variavel query do escopo com o valor (query) informado no campo busca.
         */
        $scope.$on('buscavideo', function(event, query) {
            vm.query = query;
        });

        ////////////////

        /**
         * Obter os IDs dos vídeos do youtube.
         *
         * @param {number} num Informar a quantidade máxima de vídeos a serem buscados.
         * @param {boolean} first Informar se é a primeira requisição (ou seja, ao carregar a página) (true ou false).
         */
        function getYTVideos(num, first) {
            first = (typeof first !== 'boolean') ? false : first;
            var videosIdPromise = youtubeService.getIdVideos('videos', num, first);

            videosIdPromise.then(function(result) {
                if(result.status == 200) {
                    // vm.videos = result.data.items;
                    var videosPromise = youtubeService.getYTVideos('videos', result.data.items);
                    youtubeService.setPageToken('videos', result.data.nextPageToken);
                    videosPromise.then(thenVideos);
                } 
                else console.error('Error getIdVideos(), status:'+result.status+': '+result.statusText);
            });
        }

        /**
         * Callback do promise da função "getYTVideos" do serviço "youtubeService".
         * passo final para apresentar os novos videos obtidos do youtube.
         *
         * @param {object} result Retorno 'data' da requisição que obtem os vídeos detalhados.
         */
        function thenVideos(result) {
            if(result.status == 200) {
                youtubeService.setVideos('videos', result.data.items);
                vm.videos = youtubeService.getVideos('videos');
                $timeout(function() {
                    vm.loading = false;
                }, 1000);
            } 
            else console.error('Error getYTVideos(), status:'+result.status+': '+result.statusText);
        }

    }
})(angular);