(function() {
    'use strict';

    angular
        .module('fvapp')
        .run(function(amMoment) {
		    amMoment.changeLocale('pt-br');
		});
		// .config(function ($locationProvider) {
		//   $locationProvider.html5Mode(true).hashPrefix('!');
		// });
})();